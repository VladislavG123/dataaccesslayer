﻿using System;

namespace DataAccessLayerApp.Models
{
    public class Car
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public Guid OwnerId { get; set; }
    }
}
