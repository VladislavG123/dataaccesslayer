﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayerApp
{
    public class CarDto
    {
        public string Model { get; set; }
        public string OwnerFullName { get; set; }
    }
}

