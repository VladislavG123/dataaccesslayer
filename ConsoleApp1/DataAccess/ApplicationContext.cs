﻿using DataAccessLayerApp.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayerApp.DataAccess
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(string connectionString)
            : base(new DbContextOptionsBuilder<ApplicationContext>()
                .UseSqlServer(connectionString)
                .Options)

        {
            Database.EnsureCreated();
        }

        public DbSet<Car> Cars { get; set; }
        public DbSet<Owner> Owners { get; set; }
    }
}


