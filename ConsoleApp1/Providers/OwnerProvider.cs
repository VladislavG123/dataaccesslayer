﻿using DataAccessLayerApp.DataAccess;
using DataAccessLayerApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLayerApp.Providers
{
    public class OwnerProvider : EntityProvider<ApplicationContext, Owner, Guid>
    {
        public OwnerProvider(ApplicationContext context) : base(context)
        { }

    }
}
