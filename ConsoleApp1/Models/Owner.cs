﻿using System;

namespace DataAccessLayerApp.Models
{
    public class Owner
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string FullName { get; set; }
    }
}
