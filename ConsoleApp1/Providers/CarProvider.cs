﻿using DataAccessLayerApp.DataAccess;
using DataAccessLayerApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayerApp.Providers
{
    public class CarProvider : EntityProvider<ApplicationContext, Car, Guid>
    {
        private readonly ApplicationContext _context;

        public CarProvider(ApplicationContext context) : base(context)
        {
            this._context = context;
        }

        public async Task<List<Car>> GetAllByOwnerId(Guid ownerId)
        {
            return await Get(x => x.OwnerId == ownerId);
        }

        public async Task<List<CarDto>> GetAllWithOwnerFullName()
        {
            return await (from car in _context.Cars
                          join owner in _context.Owners on car.OwnerId equals owner.Id
                          select new CarDto { Model = car.Name, OwnerFullName = owner.FullName }).ToListAsync();
        }
    }
}
