﻿using DataAccessLayerApp.DataAccess;
using DataAccessLayerApp.Models;
using DataAccessLayerApp.Providers;
using System;
using System.Collections.Generic;

namespace DataAccessLayerApp
{
    class Program
    {
        private static string _connectionString = "server =.; database = providersDB2; trusted_connection = true;";
        private static ApplicationContext _context = new ApplicationContext(_connectionString);
        private static OwnerProvider _ownerProvider;
        private static CarProvider _carProvider;

        static void Main(string[] args)
        {
            _ownerProvider = new OwnerProvider(_context);
            _carProvider = new CarProvider(_context);

            //InitializeData();

            

            Console.WriteLine("Cars:");
            ShowCars();

            Console.ReadLine();
        }


        private static async void InitializeData()
        {
            var firstOwner = new Owner
            {
                FullName = "Ivan Petrovich"
            };
            await _ownerProvider.Add(firstOwner);

            var secondOwner = new Owner
            {
                FullName = "Vasya Andreev"
            };
            await _ownerProvider.Add(secondOwner);


            var cars = new List<Car>
            {
                new Car { Name = "BMW", OwnerId = firstOwner.Id },
                new Car { Name = "Mercedes benz", OwnerId = firstOwner.Id },
                new Car { Name = "Lada", OwnerId = secondOwner.Id },
            };

            await _carProvider.AddRange(cars);
        }

        private static async void ShowCars()
        {
            foreach (var owner in await _ownerProvider.GetAll())
            {
                owner.FullName += "1";
                await _ownerProvider.Edit(owner);
            }

            foreach (var car in await _carProvider.GetAllWithOwnerFullName())
            {
                Console.WriteLine(
                    $"Model: {car.Model}. Owner: {car.OwnerFullName}");
            }
        }
    }
}
